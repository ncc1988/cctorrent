from django import template

register = template.Library()

## FILTERS:

@register.filter(name='GetAlbumDirName')
def GetAlbumDirName(album):
  try:
    return album.GetAlbumDirName()
  except:
    return -1

@register.filter(name='GetAlbumTracks')
def GetAlbumTracks(album):
  try:
    return len(album.track_set.all())
  except:
    return -1

@register.filter(name='GetArtistAlbums')
def GetArtistAlbums(artist):
  try:
    return len(artist.album_set.all())
  except:
    return -1

@register.filter(name='GetArtistTracks')
def GetArtistTracks(artist):
  try:
    return len(artist.track_set.all())
  except:
    return -1

@register.filter(name='GetTrackFileName')
def GetTrackFileName(track):
  try:
    return track.GetFileName()
  except:
    return -1
