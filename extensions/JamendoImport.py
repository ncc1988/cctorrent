#!/usr/bin/env python
#
#   This file is part of cctorrent
#   Copyright (C) 2014-2015 Moritz Strohm <ncc1988@posteo.de>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

#this extension allows the import of jamendo albums (as zip files)

import io, os, hashlib, zipfile, subprocess, datetime, urllib2
from cctorrent.models import License, Track, Album, Artist, UploadedAudioFile
from cctorrent.apps import CCTorrentConfig
from cctorrent.ConverterThread import ConverterThread
from cctorrent.helpers import convertUploadedImage
from django.contrib.auth.models import User as User
from threading import Thread
from cctorrent.CCTorrentCore import CreateArtist, CreateAlbum, CreateTrack


class JamendoImportThread(Thread):
  #def __init__(self, user=None, albumArtist=None, zipURL=None): #, albumName=None,):
  def __init__(self, user=None, albumArtistName=None, albumName=None, zipFile=None): #, zipURL=None):
    Thread.__init__(self)
    if((user == None) or (not isinstance(user, User))):
      raise Exception("JamendoImportThread: user needs to be a User instance (django.contrib.auth.models)!")
    if((albumArtistName == None) ): #or (not isinstance(albumArtistName, str))):
      raise Exception("JamendoImportThread: albumArtistName needs to be a str instance!")
    if(albumName == None):
      raise Exception("JamendoImportThread: albumName has to be set!")
    #if(zipURL == None):
    #  raise Exception("JamendoImportThread: zipURL has to be set!")
    if(zipFile == None):
      raise Exception("JamendoImportThread: zipFile has to be set!")
    self.user = user
    self.albumArtistName = albumArtistName
    self.zipFile = zipFile
    self.albumName = albumName
    #self.zipURL = zipURL
    
  def run(self):
    ImportJamendoZip(user=self.user, albumArtistName=self.albumArtistName, albumName=self.albumName, zipFile=self.zipFile) #zipURL = self.zipURL)


class JamendoLicense():
  def __init__(self, trackNumber=0, licenseURL=""):
    self.trackNumber = trackNumber
    self.licenseURL = licenseURL
    urlParts = licenseURL.split("/")
    self.license = "CC "+urlParts[4].upper() + " " + urlParts[5]
    #print ("Debug: urlParts = "+str(urlParts))
    #print ("Debug: New JamendoLicense: ("+str(self.trackNumber)+", "+self.license+", "+self.licenseURL+")")
  
  def __unicode__(self):
    return self.license

class JamendoTrack():
  def __init__(self, data = None, trackNumber=0, artist="", album="", title="", date="", license=None):
    self.data = data
    self.trackNumber = trackNumber
    self.artist = artist
    self.album = album
    self.title = title
    self.date = date
    self.license = None
    #print("DEBUG: new Track: "+unicode(self))
    
  def __unicode__(self):
    return "["+str(self.trackNumber)+"] "+self.artist + " - " + self.title + " ("+self.album+"), "+self.date + ", licensed under "+unicode(self.license)

def ParseJamendoLicenses(licenseText):
  #licenseText is a list of lines
  #helper function: this extracts the license information for each track and returns a list of JamendoLicense objects
  
  #file format:
  #header: Track <blank spaces> | <blank spaces> License URL\n
  #<line with a lot of "-" characters>
  #for each track:
  #<track number> <blank spaces> | <blank spaces> <license URL>\n
  #end of file is reached if an empty line is read
  
  licenseList = []
  del licenseText[0:2] #we don't need the headers
  for l in licenseText:
    #handle each line separatly:
    l = l.split() #we now have [<trackNumber>, "|", <licenseURL>]
    #print("Debug: "+str(l))
    licenseList.append(JamendoLicense(trackNumber=int(l[0]), licenseURL = l[2]))
  
  return licenseList
  
def GetJamendoTrackInfo(zipFile, playlist):
  #TODO: move playlistFile in here since we have zipFile
  
  trackList = []
  for p in playlist:
    #we cannot trust the playlist entries to show the right text.
    #For example cyrillic letters are replaced with _ characters in jamendo file names.
    #So we have to extract the name from the mp3 file directly using ffmpeg.
    p = p.strip('\n')
    newTrack = JamendoTrack()
    zfp = zipFile.open(p)
    newTrack.data = zfp.read()
    zfp.close()
    
    ffp = subprocess.Popen(['ffmpeg', '-i', '-'], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    fileInfo = ffp.communicate(newTrack.data) #TODO: we only need to pass the first 64 KiB of the file to ffmpeg:
    
    #ok, we have read the stderr output of ffmpeg. Now we have to parse that:
    fileInfo = fileInfo[1].split('\n')
    fields = []
    fieldIndexes = []
    for l in fileInfo:
      l = l.split(':',1)
      l[0]=str(l[0]).strip()
      if(len(l)>1):
        l[1]=(str(l[1]).lstrip())
        fieldIndexes.append(l[0])
        fields.append(l[1])
    
    #we now have two arrays, one with the keys and one with the values (which have the same length)
    #so we can cut them at the same place:
    fields = fields[fieldIndexes.index("Metadata")+1:]
    fieldIndexes = fieldIndexes[fieldIndexes.index("Metadata")+1:]
    
    #now we have the metadata at the begin of the list
    
    
    #TODO: better exceptions if metadata are missing!
    
    try:
      #we now extract all metadata we want to know:
      newTrack.trackNumber = int(fields[fieldIndexes.index("track")].lstrip('0'))
      newTrack.title = fields[fieldIndexes.index("title")]
      newTrack.artist = fields[fieldIndexes.index("artist")]
      newTrack.album = fields[fieldIndexes.index("album")]
      newTrack.date = fields[fieldIndexes.index("TDTG")]
    except Exception as e:
      print("ERROR: Cannot parse metadata of file "+zipFile.filename)
    trackList.append(newTrack)
  
  return trackList



def ImportJamendoZip(user, albumArtistName, albumName, zipFile): #zipURL):
  #albumArtistName is the artist/composer name that may be in the database
  
  
  #zipFile = None
  try:
    #urlFile = urllib2.urlopen(zipURL)
    #zipFile = zipfile.ZipFile(io.BytesIO(urlFile.read()))
    zipFile = zipfile.ZipFile(io.BytesIO(zipFile.read())) #TODO: check if this is necessary
  except Exception as e:
    raise Exception("Error occured: "+str(e))
  
  #fileList = zipFile.infolist()
  #print(fileList)
  
  playlistFile = zipFile.open("playlist.m3u") #jamendo playlists are just file lists (same output as with ls -1 *.mp3)
  playlist = playlistFile.readlines()
  playlistFile.close()
  
  #print ("Debug: Handling playlist:")
  trackList = GetJamendoTrackInfo(zipFile, playlist)
  
  licenseFile = zipFile.open("License.txt")
  licenseList = ParseJamendoLicenses(licenseFile.readlines())
  licenseFile.close()
  
  #tracks and licenses ought to be in alphabetical order and with the same length:
  for t in trackList:
    t.license = licenseList[trackList.index(t)]
    
    #print (unicode(t))
  
  #now we have our track list ready for CCTorrent:
  
  #first import the licenses if they're not in the database:
  #the JamendoTrackInfo objects will contain DB License objects after this for-loop
  for t in trackList:
    try:
      dbl = License.objects.get(short_name=t.license.license)
      t.license = dbl
    except License.DoesNotExist as d:
      newLicense = License()
      newLicense.short_name = t.license.license
      newLicense.name = t.license.license #TODO: construct name based on short name
      newLicense.url = t.license.licenseURL
      newLicense.save()
      t.license = newLicense
  
  #all licenses are imported.
  
  #get the artist ID (or create the artist if its not in the database):
  try:
    artist = Artist.objects.get(name=albumArtistName)
  except Artist.DoesNotExist:
    artist = CreateArtist(user=user, artistName=albumArtistName)
  
  #artist = albumArtist
  
  print("DEBUG: Album File")
  
  #create the album:
  #open the album cover file:
  coverFile = zipFile.open("Cover.jpg")
  #a temporary workaround:
  newAlbum = Album.objects.get(pk=CreateAlbum(name = albumName,
					      publicationDate = datetime.datetime.now(),
					      licenseShort = trackList[0].license.short_name, #TODO: make this configurable
					      albumCover = io.BytesIO(coverFile.read()),
					      artistId=artist.id
					      )
    )
  coverFile.close()
  
  print("DEBUG: Tracks!")
  
  #Now let's create the necessary entries for each track:
  for t in trackList:
    #check if the artist exists and create it if not:
    tartist = None
    try:
      tartist = Artist.objects.get(name=t.artist)
    except Artist.DoesNotExist:
      tartist = CreateArtist(user=user, artistName = t.artist)
    
    #the track should not exist:
    ttrack = CreateTrack(name=t.title,
                            number=t.trackNumber,
                            albumId=newAlbum.id,
                            artistId=tartist.id,
                            licenseShort=t.license.short_name,
                            trackData=t.data
                            )
  
  #we're finished:
  zipFile.close()
  return 0
  