#!/usr/bin/env python
# -*- coding: utf-8 -*-

#   This file is part of cctorrent
#   Copyright (C) 2014-2015 Moritz Strohm <ncc1988@posteo.de>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from django.apps import AppConfig
from django.conf import settings as django_settings


class CCTorrentConfig(AppConfig):
  #for admin:
  name = 'cctorrent'
  nerbose_name = "CCTorrent – A django application combining CreativeCommons music and BitTorrent"
  
  #config options:
  DemoMode = True #if this is set to true then the torrents are created as private torrents and there are more tooltips (planned)
  AlbumFileDirectory = '/var/www/cctorrent/albums/'
  AlbumWebDirectory = '/albums/'
  SquareImageSize = 400 #Image size of album and artist images which are in square format (width = height)
  ImageQuality = 90 #Quality for images
  FileDirectory = django_settings.MEDIA_ROOT+'cctorrent/'
  FileURL = django_settings.MEDIA_URL+'cctorrent/'
  AudioBitrate = 160 #bitrate in kbit/s
  MinimumArtistNameLength = 4
  MinimumAlbumNameLength = 4
  MinimumTrackNameLength = 4
  MinimumTagLength = 3
  