# -*- coding: utf-8-*-


from threading import Thread
#from helpers import convertUploadedTrack
from models import UploadedAudioFile
import subprocess
from cctorrent.apps import CCTorrentConfig

class ConverterThread(Thread):
  
  def __init__(self, uploadedFile=None):
    Thread.__init__(self)
    if((uploadedFile == None) or (not isinstance(uploadedFile, UploadedAudioFile))):
      raise Exception("ConverterThread must be initialised with a valid UploadedAudioFile object!")
    
    self.uploadedFile = uploadedFile
    self.daemon = True #probably this is bad because the app doesn't check if the database is accessed when the thread is closed unexpectedly
  
  def run(self):
    if(self.uploadedFile.uploaded == True):
      print("DEBUG: Converter thread started for file "+self.uploadedFile.fileName+ "...")
      #$self.uploadedFile is a database model object
      
      #print ("DEBUG: artist=["+unicode(u'artist='+self.uploadedFile.track.artist.name)+"]")
      
      args = ['ffmpeg', 
        '-v','0',
        '-i', unicode(CCTorrentConfig.FileDirectory+'convert/'+self.uploadedFile.fileName), '-vn', '-sn', '-codec:a', 'libvorbis', '-b:a', str(CCTorrentConfig.AudioBitrate)+'k', '-f', 'ogg',
        '-map_metadata','-1',
        #begin metadata:
        '-metadata',u'title='+unicode(self.uploadedFile.track.name),
        '-metadata',u'artist='+unicode(self.uploadedFile.track.artist.name),
        '-metadata',u'album='+unicode(self.uploadedFile.album.name),
        '-metadata',u'track='+unicode(self.uploadedFile.track.number),
        '-metadata',u'license='+unicode(self.uploadedFile.track.license),
        '-metadata',u'date='+unicode(self.uploadedFile.album.pub_date),
        '-metadata',u'encoder=CCTorrent',
        '-metadata',u'comment=This file is distributed via CCTorrent, a torrent software for Creative Commons music.',
        #end metadata
        '-y', CCTorrentConfig.FileDirectory+'album/'+str(self.uploadedFile.album.GetAlbumDirName())+'/'+self.uploadedFile.track.GetFileName()
        ]
        
      try:
        ffmpeg = subprocess.call(args, shell=False)
        self.uploadedFile.converted = True
      except Exception as e:
        self.uploadedFile.errorOccured = True
        self.uploadedFile.error = e.msg
      
      self.uploadedFile.save()
      
      print("DEBUG: Converter thread finished for file "+self.uploadedFile.fileName+ "...")
    else:
      print("ERROR: Converter cannot work with not uploaded file "+self.uploadedFile.fileName+ "...")
      #TODO: raise Exception




class ListConverterThread(Thread):
  #converts everything that gets passed to it in sequential order
  def __init__(self, threadList=None):
    Thread.__init__(self)
    if((threadList == None) or (not isinstance(threadList, list))):
      raise Exception("ListConverterThread requires a list of ConverterThread objects!")
    self.threadList = threadList
    self.daemon = True
  
  def run(self):
    for t in self.threadList:
      #we want to do the tasks in sequence
      t.run()
