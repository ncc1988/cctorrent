from django import forms
from cctorrent.models import License

class NewTrackForm(forms.Form):
  name = forms.CharField(max_length=100)
  #TODO: license selection and file upload!
  licenseShort = forms.ModelChoiceField(queryset=None)
  musicFile = forms.FileField() #TODO: check for wav or ogg vorbis or flac!
  number = forms.IntegerField()
  tags = forms.CharField(max_length=100)
  
  def __init__(self, *args, **kwargs):
    super(NewTrackForm, self).__init__(*args, **kwargs)
    self.fields['licenseShort'].queryset = License.objects.all() #licenseList