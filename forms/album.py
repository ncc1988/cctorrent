from django import forms
from cctorrent.models import License

class NewAlbumForm(forms.Form):
  name = forms.CharField(max_length=100)
  cover = forms.ImageField()
  publicationDate = forms.DateTimeField()
  licenseShort = forms.ModelChoiceField(queryset=None)
  tags = forms.CharField(max_length=100)
  
  def __init__(self, *args, **kwargs):
    super(NewAlbumForm, self).__init__(*args, **kwargs)
    self.fields['licenseShort'].queryset = License.objects.all() #licenseList