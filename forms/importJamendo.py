from django import forms
from cctorrent.models import Artist


class JamendoImportForm(forms.Form):
  #albumArtist = forms.ModelChoiceField(queryset=None, label="Album artist")
  albumArtistName = forms.CharField(max_length=256, label="Album artist")
  albumName = forms.CharField(max_length=256, label="Album name")
  zipFile = forms.FileField()
  #zipURL = forms.URLField(label="Album zip file URL")
  
  def __init__(self, *args, **kwargs):
    super(JamendoImportForm, self).__init__(*args, **kwargs)
    #self.fields['albumArtist'].queryset = Artist.objects.all()