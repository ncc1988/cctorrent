from django import forms

class NewArtistForm(forms.Form):
  name = forms.CharField(max_length=100)
  description = forms.CharField()
  picture = forms.ImageField()
  tags = forms.CharField(max_length=100)
  cryptocoinAddress = forms.CharField(required=False) #not yet in artist or user database model!!
  
