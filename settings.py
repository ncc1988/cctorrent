#CCTorrent specific configuration variables

#deactivated, replaced by the django 1.7 settings module
#from django.conf import settings as django_settings

#CCTORRENT_FILEDIR = '/var/www/cctorrent/' #the path where media files are stored on this server
#CCTORRENT_FILEURL = 'http://127.0.0.1/cctorrent/' #the URL from which the media files can be accessed by everyone

#settings = {
  #'SquareImageSize':400, #Image size of album and artist images which are in square format (width = height)
  #'ImageQuality':90, #Quality for images
  #'FileDirectory':django_settings.MEDIA_ROOT+'cctorrent/',
  #'FileURL':django_settings.MEDIA_URL+'cctorrent/',
  #'AudioBitrate':160, #bitrate in kbit/s
  #}