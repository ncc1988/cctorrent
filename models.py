# -*- coding: utf-8 -*-
#
#   This file is part of cctorrent
#   Copyright (C) 2014-2015 Moritz Strohm <ncc1988@posteo.de>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator, MaxValueValidator
#from django.db.models.signals import post_save


#TODO: reactivate if possible:
#class CCTorrentUser(models.User):
  #karma = models.IntegerField(default=0)
  #def __unicode__(self):
    #return self.username+" (karma: "+str(self.karma)+")"
    
  #def createCCTorrentUser(sender, instance, created, **kwargs):
    #if created:
     #CCTorrentUser.objects.create(user=instance)
  
  #post_save.connect(createCCTorrentUser, sender=User)


class Rating(models.Model):
  stars = models.PositiveIntegerField(validators=[MinValueValidator(1), MaxValueValidator(5)])
  comment = models.CharField(max_length=512)
  #TODO: add user reference (so that a user can't rate twice)
  #TODO: add ManyToManyFields in artist, album and track

class DonationInfo(models.Model):
  #place donation information into $description (e.g. Name+IBAN+BIC, Litecoin address,...)
  description = models.CharField(max_length=320)

class Tag(models.Model):
  name = models.CharField(primary_key=True, max_length=24)
  
  def __unicode__(self):
    return unicode(self.name)
  
class Artist(models.Model):
  name = models.CharField(max_length=256, unique=True)
  user = models.ForeignKey(User)
  description = models.TextField(default='')
  tags = models.ManyToManyField(Tag)
  def __unicode__(self):
    return unicode(self.name) + " (user: " + unicode(self.user.username) + ")"

class License(models.Model):
  short_name = models.CharField(max_length=16, unique=True)
  name = models.CharField(max_length=256)
  url = models.CharField(max_length=512)
  def __unicode__(self):
    return unicode(self.short_name)


class Album(models.Model):
  artist = models.ForeignKey(Artist) #TODO: rename to composer (or another word for someone who arranged the album)
  #TODO: add field artists (if more than one artist is present in the album)
  name = models.CharField(max_length=256)
  license = models.ForeignKey(License)
  pub_date = models.DateTimeField()
  finished = models.BooleanField(default=False) #if an album is finished it and its tracks can't be changed anymore
  tags = models.ManyToManyField(Tag)
  def __unicode__(self):
    return unicode(self.name)+" ("+unicode(self.artist.name)+")"
  
  def GetAlbumDirName(self):
    return self.artist.name + ' - ' + self.name

class Track(models.Model):
  name = models.CharField(max_length=256)
  artist = models.ForeignKey(Artist)
  #TODO: add field artists (if more than one artist made the track)
  license = models.ForeignKey(License)
  album = models.ForeignKey(Album)
  tags = models.ManyToManyField(Tag)
  number = models.IntegerField(default=0) #track number in album
  def __unicode__(self):
    return '{:02d}'.format(self.number) + " " + unicode(self.artist.name) + " - " + unicode(self.name)
    
  def formattedNumber():
    return '{:02d}'.format(self.number)
  
  def GetFileName(self):
    return '{:02d}'.format(self.number)+' '+str(self.name)
  
class UploadedAudioFile(models.Model):
  #this holds the path to an uploaded but not yet converted audio file
  #plus, all relevant information are stored in here to place the converted file in the right place
  fileName = models.CharField(max_length=4096)
  track = models.OneToOneField(Track, related_name="uploadedFile") #artist can be derived from track
  album = models.ForeignKey(Album) #a track may be placed in one or multiple albums
  uploaded = models.BooleanField(default=False)
  converted = models.BooleanField(default=False)
  errorOccured = models.BooleanField(default=False)
  errorStr = models.CharField(max_length=1024, default="")
  
  def __unicode__(self):
    string = self.fileName+' ('
    if(self.uploaded):
      string += "U"
    else:
      string += "u"
    if(self.converted):
      string += "C"
    else:
      string += "c"
    if(self.errorOccured):
      string += "E"
    else:
      string += "e"
    string += ')'
    return string


class RegisteredExtension(models.Model):
  #extensions have their own database table with important details about them
  uniqueName = models.CharField(max_length=256, unique=True) #can be: org.example.superExtension or simply a_new_extension or TheBestExtension or ...
  name = models.CharField(max_length=128) #the name that is shown to the user
  versionMajor = models.PositiveIntegerField(default=0)
  versionMinor = models.PositiveSmallIntegerField(default=0)
  
  homepage = models.URLField(max_length=256)
  author = models.CharField(max_length=192)
  authorMail = models.CharField(max_length=128, null=False)
  requiredCCTorrentAPIVersion = models.PositiveIntegerField(default=0) #the API version of CCTorrent that is required for an extension to work
  
  #TODO: extension dependencies (requires brain activity!)
  #dependencies = models.

class ExtensionSetting(models.Model):
  #extensions may store their configuration in the database as key-value pair
  extension = models.ForeignKey(RegisteredExtension)
  key = models.CharField(max_length=256, null=False)
  value = models.CharField(max_length=512, null=False)
