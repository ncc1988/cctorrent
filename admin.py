#
#   This file is part of cctorrent
#   Copyright (C) 2014 Moritz Strohm <ncc1988@posteo.de>
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero General Public License as
#   published by the Free Software Foundation, either version 3 of the
#   License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero General Public License for more details.
#
#   You should have received a copy of the GNU Affero General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
#


from cctorrent.models import Artist,License,Album,Track,Tag,DonationInfo,Rating,UploadedAudioFile
from django.contrib import admin

#admin.site.register(CCTorrentUser)
admin.site.register(Artist)
admin.site.register(License)
admin.site.register(Album)
admin.site.register(Track)

admin.site.register(Tag)
admin.site.register(DonationInfo)
admin.site.register(Rating)

admin.site.register(UploadedAudioFile)
